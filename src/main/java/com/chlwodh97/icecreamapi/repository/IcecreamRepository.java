package com.chlwodh97.icecreamapi.repository;

import com.chlwodh97.icecreamapi.entity.Icecream;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IcecreamRepository extends JpaRepository<Icecream , Long> {
}
